import logo from './logo.svg';
import './App.css';
import SideBarComponent from './components/SideBarComponent';
import MainComponent from './components/MainComponent';
import { RigthComponent } from './components/RigthComponent';

function App() {
  return (
    <div className="grid grid-cols-12 bg-white">
        <div className='col-span-1'>
          <SideBarComponent />
        </div>
        <div className='col-span-8'>
          <MainComponent />
        </div>
        <div class="bg-rightbg bg-cover bg-center col-span-3">
          <RigthComponent/>
        </div>
     
    </div>
  );
}

export default App;
