import React from 'react'

export const RigthComponent = () => {
  return (
    <div >
      <div>
        <div className='inline-flex p-8 ml-16'>
          <img className='w-6 h-6 ml-4' src="./Images/notification.png" />
          <img className='w-6 h-6 ml-4' src="./Images/messenger.png" />
          <img className='w-6 h-6 ml-4 rounded-full' src="./Images/lachlan.jpg" />
        </div>
        {/* button  */}
        <div>
          <button className='float-right mr-10 bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded'>My amazing trip</button>
        </div>
        {/* paragraph */}
        <div>
          <p className='float-left font-bold f text-lg text-left p-10 bold'>I like slaying down on the sand and look at the moon.</p>
        </div>
        <div className='float-left pl-10  text-[#00337C]'>
          27 people going to this trip
        </div>
      </div>
      <div className='p-5 float-left inline-flex'>
        <img className='w-10 h-10 ml-4 rounded-full border-2 border-white' src="./Images/lachlan.jpg" />
        <img className='w-10 h-10 ml-4 rounded-full border-2 border-green-500' src="./Images/raamin.jpg" />
        <img className='w-10 h-10 ml-4 rounded-full border-2 border-green-500' src="./Images/christina.jpg" />
        <img className='w-10 h-10 ml-4 rounded-full border-2 border-green-500' src="./Images/nonamesontheway.jpg" />
        <p className='flex justify-center items-center text-xs text-white font-[Poppins] font-bold bg-blue-300 w-10 h-10 ml-4 rounded-full border-2 border-yellow-600'>23+</p>
      </div>
    </div>
  )
}
