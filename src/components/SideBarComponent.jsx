import React from 'react'
const SideBarComponent = () => {
    return (
        <div>
            <div class="flex flex-row min-h-full h-[100vh]">
                <nav class="bg-gray-300 w-20  justify-between flex flex-col ">
                    <div class="mt-10 mb-10">
                        <a href="#">
                            <img
                                src="Images/category_icon.png"
                                class="rounded-full w-10 h-10 mb-3 mx-auto"
                            />
                        </a>
                        <div class="mt-10 flex-col grap-6">
                            <ul>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/cube.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />
                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/list.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />

                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <p className='bg-red-600 w-2 h-2 rounded-full absolute mt-[162.5px] ml-3 border border-white-500'></p>
                                        <span>
                                            <img src="Images/messenger.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span>
                                            <img src="Images/list.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="mt-10 flex-col grap-6">
                            <ul>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/success.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />
                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/security.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />
                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/users.png" alt="" class="fill-current h-8 w-8 mx-auto text-gray-300 hover:text-green-500" />
                                        </span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                        <div class="mt-10 flex-col grap-6">
                            <ul>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/christina.jpg" alt="" class="rounded-full w-[25px] h-[26px] mx-auto" />
                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/lachlan.jpg" alt="" class="rounded-full w-[25px] h-[26px] mx-auto" />
                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/nonamesontheway.jpg" alt="" class="rounded-full w-[25px] h-[26px] mx-auto" />
                                        </span>
                                    </a>
                                </li>
                                <li class="mb-6">
                                    <a href="#">
                                        <span>
                                            <img src="Images/plus.png" alt="" class="rounded-full w-[25px] h-[26px] mx-auto" />
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    )
}

export default SideBarComponent