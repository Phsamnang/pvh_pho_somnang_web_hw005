import React, { useState } from 'react'
const ModelInputComponent = ({ data, setData }) => {
    const [newData, setNewData] = useState([]);
    const onChangeHandler = (e) => {
        setNewData({
            ...newData, [e.target.name]: e.target.value
        })
    }
    console.log(newData);
    const submitHandler = (e) => {
        e.preventDefault();
        setData([...data, { id: data.length + 1, ...newData }])
        console.log(data);
    }
    return (
        <div>
            {/* The button to open modal */}
            <label htmlFor="my-modal-2" className="btn">ADD NEW TRIP</label>
            {/* Put this part before </body> tag */}
            <form onSubmit={submitHandler}>
                <input type="checkbox" id="my-modal-2" className="modal-toggle" />
                <div className="modal">
                    <div className="modal-box relative bg-white">
                        <label htmlFor="my-modal-2" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>

                        <div class="mb-6">
                            <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Title</label>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Title" name='title' onChange={onChangeHandler} />
                        </div>
                        <div class="mb-6">
                            <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Description</label>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Description" name='description' onChange={onChangeHandler} />
                        </div>
                        <div class="mb-6">
                            <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">People going</label>
                            <input type="text" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Number of people going" name='peopleGoing' onChange={onChangeHandler} />
                        </div>
                        <div className='mb-6'>
                            <label for="countries" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Type of advanture</label>
                            <select id="countries" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" name='status' onChange={onChangeHandler}>
                                <option disabled>Choose any option</option>
                                <option value='beach'>Beach</option>
                                <option value='mountain'>Mountain</option>
                                <option value='forest'>Forest</option>
                            </select>
                        </div>
                        <div className="modal-action flex justify-start">
                            <button type="submit">
                                <label htmlFor="my-modal-2" className='btn text-white bg-blue-700 hover:bg-blue-800 font-medium rounded-lg text-sm  py-2.5 mr-2 mb-2 w-32'>
                                    Add New
                                </label>
                            </button>

                        </div>
                    </div>
                </div>
            </form>

        </div >
    )
}
export default ModelInputComponent