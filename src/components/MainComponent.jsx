import React, { useState } from 'react'
import CardComponent from './CardComponent'
import ModelInputComponent from './ModelInputComponent';
const MainComponent = () => {
  const [data, setData] = useState([
    { id: 1,
      title: "koh kong krav",
      description:
        "Koh Kong Krav Beach is in the 5th place out of 13 beaches in the Koh Kong region The beach is located in a natural place, among the mountains. It is partially covered 	   by trees which give natural shade. It is a spacious coastline with crystal turquoise water and white fine sand, so you don't need special shoes.",
      status: "beach",
      peopleGoing: "1537",
    },
    {
      id: 2,
      title: "phnom sampov",
      description:
        " This legendary 100 metres high mountain, topped by Wat Sampeou, contains 3 natural caves, lined with Buddhist shrines and statues: Pkasla, Lakhaon and Aksopheak.",
      status: "mountain",
      peopleGoing: "81000",
    },
    {
      id: 3,
      title: "kirirom",
      description:
        "Kirirom National Park, a high altitude plateau, is known for its unique high elevation pine forest, which forms the headwaters for numerous streams feeding Kampong 	   	   Speu Town.",
      status: "forest",
      peopleGoing: "2500",
    }
  ])
  let time = new Date().getHours(); 
  return (
    <div className='mr-5 mt-10 '>
      <div className='flex justify-between'>
        <h1 className='text-2xl text-black'>Good {time>=5&&time<12?'Morning':time>=12&&time<17?"Afternoon":time>=17&&time<21?"Evening":"Night"} Team!</h1>
        <ModelInputComponent data={data} setData={setData}/>
      </div>
      <CardComponent data={data} setData={setData}/>
    </div>
  )
}

export default MainComponent
