import React, { useState } from 'react'

const CardComponent = ({ data, setData }) => {
    const [detail, setDetail] = useState([]);
    const changeStatus = (idx) => {
        if (data[idx].status === "forest") {
            data[idx].status = "beach"
        } else if (data[idx].status === "beach") {
            data[idx].status = "mountain"
        } else {
            data[idx].status = "forest"
        }
        setData([
            ...data
        ])
    }
    return (
        <div className='grid grid-cols-3'>
            {data.map((item, index) => (
                <div className='m-2'>
                    <div class="max-w-sm p-6 bg-gray-800 border border-gray-800 rounded-lg shadow">
                        <h5 class="mb-2 text-2xl font-bold tracking-tight text-white uppercase">{item.title}</h5>
                        <p class="mb-3 font-normal text-white font-semibold font-[Poppins] line-clamp-3">{item.description}</p>
                        <div className='flex-col'>
                            <p className='mb-3 font-normal text-white'>
                                People going
                            </p>
                            <p className='mb-3 font-normal text-white'>
                                {item.peopleGoing}
                            </p>
                        </div>
                        <div className='flex justify-between'>
                            <button type="button" className={item.status === 'beach' ?
                                "focus:outline-none text-white bg-blue-700 hover:bg-blue-800 font-medium rounded-lg text-sm  py-2.5 mr-2 mb-2 w-32 uppercase"
                                : item.status === 'mountain' ? "focus:outline-none text-white bg-gray-400 hover:bg-gray-500  font-medium text-sm rounded-lg py-2.5 mr-2 mb-2 w-32 uppercase" :
                                    "focus:outline-none text-white bg-green-700 hover:bg-green-800 font-medium rounded-lg text-sm w-32 py-2.5 mr-2 mb-2 uppercase"} onClick={() => changeStatus(index)}>{item.status}</button>
                            <label htmlFor="my-modal-3" className="btn text-sm  py-2.5 mr-2 mb-2 w-32"
                                onClick={() => setDetail(item)}>READ DETAILS</label>
                            <input type="checkbox" id="my-modal-3" className="modal-toggle" />
                            <div className="modal">
                                <div className="modal-box relative bg-white">
                                    <label htmlFor="my-modal-3" className="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
                                    <h3 className="text-lg font-bold text-black uppercase" >{detail.title}</h3>
                                    <p className="py-4 font-semibold font-[Poppins]" >{detail.description}</p>
                                    <p className="py-4" >Around <span className='font-bold text-black'>{detail.peopleGoing}</span> poeple going</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            ))}

        </div>

    )
}

export default CardComponent